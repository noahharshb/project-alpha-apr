from django.contrib import admin
from projects.models import Project


# Register your models here.
@admin.register(Project)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "owner",
    )
